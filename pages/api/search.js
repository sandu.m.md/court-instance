const models = require('../../db/models')
const Sequelize = require('sequelize')
// var sequelize = new Sequelize(/* ... */);
// const Dossier = require('../../db/models/dossier')(sequelize, sequelize.DataTypes)
const Op = Sequelize.Op

export default async function handler(req, res) {
  // console.log(Dossier)

  const dossiers = await models.Dossier.findAll({
    attributes: ['id', 'name'],
    where: {
        name: {
            [Op.like]: '%'+req.body.query+'%'
        }
    },
    limit: 10
  })

  // console.log(dossiers)
  // console.log(metadata)
  res.status(200).json(dossiers)
}