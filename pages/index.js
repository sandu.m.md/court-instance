import { useState } from 'react'
import Head from 'next/head'
import Router from 'next/router'

let suggestions = []

export default function Home() {
  const [query, setQuery] = useState('')
  const [activeSuggestionIndex, setActiveSuggestionIndex] = useState(-1)
  const [refresh, setRefresh] = useState(false)

  const handleChange = async (query) => {
    if (query == '') {
      suggestions = []
      setQuery('')
      return
    }

    setQuery(query)
    setActiveSuggestionIndex(-1)

    const resp = await fetch('/api/search', {
      method: 'POST',
      body: JSON.stringify({
        query: query
      }),
      headers: {
        'Content-Type': 'application/json' 
      }
    })

    suggestions = await resp.json()
    setRefresh(!refresh)
  }

  // console.log(activeSuggestionIndex);

  const handleKeyPress = async (e) => {
    
    
    if (e.keyCode == 38) {
      let newActiveSuggestionIndex = activeSuggestionIndex - 1
    
      if (newActiveSuggestionIndex < 0) {
        newActiveSuggestionIndex = suggestions.length - 1
      }

      setActiveSuggestionIndex(newActiveSuggestionIndex)
      e.preventDefault()
    } else if (e.keyCode == 40) {
      let newActiveSuggestionIndex = activeSuggestionIndex + 1

      if (newActiveSuggestionIndex >= suggestions.length) {
        newActiveSuggestionIndex = -1
      }

      setActiveSuggestionIndex(newActiveSuggestionIndex)
      e.preventDefault()
    } else if (e.keyCode == 13) {
      if (activeSuggestionIndex >= 0 && activeSuggestionIndex < suggestions.length) {
        // handleChange(suggestions[activeSuggestionIndex].name)()
        Router.push('/search?query=' + encodeURIComponent(suggestions[activeSuggestionIndex].name))
      }
    }
  }

  const haveSuggestions = suggestions.length > 0 ? true : false;

  return (
    <div className="wrapper">
      <Head>
        <title>Search Instance</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500;700&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
      </Head>

      <div className="search-bar-wrapper">
        <h1>Instance</h1>
        <div style={{position: 'relative'}}>
          <input
            type="text"
            className="search-bar-input"
            placeholder="Type to search..."
            value={query}
            onChange={e => handleChange(e.target.value)}
            onKeyDown={e => handleKeyPress(e)}
          />
          <div className="search-bar-icon-wrapper" style={{position:'absolute',right:0,top:0}}>
            <i className="fa fa-search search-bar-icon"></i>
          </div>
          <div style={{position: 'absolute', width: '100%', height: 0}}>
            <div style={{position: "relative", width: '100%', height: 0}}>
              <div className={"search-box" + (haveSuggestions ? '' : ' hidden')}>
                {suggestions.map((suggestion, key) => {
                  let str = suggestion.name.replaceAll('ă', 'a')
                  str = str.replaceAll('â', 'a')
                  str = str.replaceAll('î', 'i')
                  str = str.replaceAll('ș', 's')
                  str = str.replaceAll('ț', 't')
                  str = str.replaceAll('Ă', 'A')
                  str = str.replaceAll('Â', 'A')
                  str = str.replaceAll('Î', 'I')
                  str = str.replaceAll('Ș', 'S')
                  str = str.replaceAll('Ț', 'T')
                  str = str.toUpperCase()
                  let qr = query.toUpperCase()

                  if (key == activeSuggestionIndex) {
                    return <div key={suggestion.id} title={suggestion.name} className="search-suggestion search-suggestion-active">
                      <span>{suggestion.name.substr(0, str.indexOf(qr))}</span>
                      <strong>{suggestion.name.substr(str.indexOf(qr), qr.length)}</strong>
                      <span>{suggestion.name.substr(str.indexOf(qr) + qr.length)}</span>
                    </div>  
                  }

                  return <div key={suggestion.id} title={suggestion.name} className="search-suggestion">
                    <span>{suggestion.name.substr(0, str.indexOf(qr))}</span>
                    <strong>{suggestion.name.substr(str.indexOf(qr), qr.length)}</strong>
                    <span>{suggestion.name.substr(str.indexOf(qr) + qr.length)}</span>
                  </div>
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
